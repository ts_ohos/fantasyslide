package com.github.mzule.fantasyslide.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.app.Context;

/**
 * Created by CaoDongping on 9/6/16.
 */

public class UniversalActivity extends AbilitySlice {


    public static Intent newIntent(Context context, String title) {
//        Intent intent = new Intent(context, UniversalActivity.class);
//        intent.putExtra("title", title);
        Intent intent = new Intent();
        intent.setParam("title", title);
        return intent;
    }

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
    }

    @Override
    public Component findComponentById(int resID) {
        Component v = super.findComponentById(resID);
        return v;
    }
}
