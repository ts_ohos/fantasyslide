package com.github.mzule.fantasyslide.slice;

import com.github.mzule.fantasyslide.*;
import com.github.mzule.fantasyslide.widget.RoundImage;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.agp.window.dialog.ToastDialog;
import ohos.app.Context;
import ohos.global.resource.Element;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends AbilitySlice implements Component.ClickedListener {

    private Button btnSwitch;
    private SideBar leftMenu;
    private Image mainImage;
    private RoundImage roundImage;
    private Text text_1;
    private Text text_2;
    private Text text_3;
    private Text text_4;
    private Text text_5;
    private Text text_6;
    private Text text_7;
    private DirectionalLayout userInfoLayout;
    private Text text_pyq;
    private Text text_money;
    private Text text_yhq;
    private Text text_friend;
    private Text text_setting;
    private Color textColor;
    private FantasyDrawerLayout fantasyDrawerLayout;

    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        setTransformer();
        fantasyDrawerLayout =
                (FantasyDrawerLayout) findComponentById(ResourceTable.Id_drawerLayout);
        leftMenu = (SideBar) findComponentById(ResourceTable.Id_leftSideBar);
        mainImage = (Image) findComponentById(ResourceTable.Id_mainImage);
        roundImage = (RoundImage) findComponentById(ResourceTable.Id_roundImage);
        Image image = new Image(getContext());
        image.setPixelMap(ResourceTable.Media_me);
        PixelMap thumbImage = image.getPixelMap();
        roundImage.setPixelMapAndCircle(thumbImage);

        initClick();
        fantasyDrawerLayout.attachComponent(mainImage, btnSwitch);
    }

    private void initClick() {
        btnSwitch = (Button) findComponentById(ResourceTable.Id_btn_switch);
        text_1 = (Text) findComponentById(ResourceTable.Id_text_1);
        text_2 = (Text) findComponentById(ResourceTable.Id_text_2);
        text_3 = (Text) findComponentById(ResourceTable.Id_text_3);
        text_4 = (Text) findComponentById(ResourceTable.Id_text_4);
        text_5 = (Text) findComponentById(ResourceTable.Id_text_5);
        text_6 = (Text) findComponentById(ResourceTable.Id_text_6);
        text_7 = (Text) findComponentById(ResourceTable.Id_text_7);
        userInfoLayout = (DirectionalLayout) findComponentById(ResourceTable.Id_userInfo);
        text_pyq = (Text) findComponentById(ResourceTable.Id_text_pyq);
        text_money = (Text) findComponentById(ResourceTable.Id_text_money);
        text_yhq = (Text) findComponentById(ResourceTable.Id_text_yhq);
        text_friend = (Text) findComponentById(ResourceTable.Id_text_friend);
        text_setting = (Text) findComponentById(ResourceTable.Id_text_setting);
        btnSwitch.setClickedListener(this);
        text_1.setClickedListener(this);
        text_2.setClickedListener(this);
        text_3.setClickedListener(this);
        text_4.setClickedListener(this);
        text_5.setClickedListener(this);
        text_6.setClickedListener(this);
        text_7.setClickedListener(this);
        userInfoLayout.setClickedListener(this);
        text_pyq.setClickedListener(this);
        text_money.setClickedListener(this);
        text_yhq.setClickedListener(this);
        text_friend.setClickedListener(this);
        text_setting.setClickedListener(this);
        textColor = text_1.getTextColor();
    }

    private void setListener() {
        final Text tipView = (Text) findComponentById(ResourceTable.Id_tipView);
        SideBar leftSideBar = (SideBar) findComponentById(ResourceTable.Id_leftSideBar);
        leftSideBar.setFantasyListener(new SimpleFantasyListener() {
            @Override
            public boolean onHover(Component view, int index) {
                tipView.setVisibility(Component.VISIBLE);
                if (view instanceof Text) {
                    tipView.setText(String.format("%s at %d",
                            ((Text) view).getText().toString(), index));
                } else if (view != null && view.getId() == ResourceTable.Id_userInfo) {
                    tipView.setText(String.format("个人中心 at %d", index));
                } else {
                    tipView.setText(null);
                }
                return false;

            }

            @Override
            public boolean onSelect(Component view, int index) {
                tipView.setVisibility(Component.INVISIBLE);
                new ToastDialog(MainActivity.this)
                        .setText(String.format("%d selected", index))
                        .setDuration(0)
                        .show();
                return false;
            }

            @Override
            public void onCancel() {
                tipView.setVisibility(Component.INVISIBLE);
            }
        });
    }

    private void setTransformer() {
        try {
            final float spacing = getResourceManager()
                            .getElement(ResourceTable.Integer_spacing).getInteger();
            final int selectColor = getResourceManager()
                    .getElement(ResourceTable.Color_color_1).getColor();
            SideBar rightSideBar = (SideBar) findComponentById(ResourceTable.Id_rightSideBar);
            rightSideBar.setTransformer(new Transformer() {

                private Component lastHoverView;
                private Text text;

                @Override
                public void apply(ComponentContainer sideBar, Component itemView,
                                  float touchY, float slideOffset, boolean isLeft) {
                    boolean hovered = itemView.isPressed();
                    if (hovered) {
                        ((Text) itemView).setTextColor(new Color(selectColor));
                    }
                    if (hovered && lastHoverView != itemView) {
                        animateIn(itemView);
                        animateOut(lastHoverView);
                        if (slideOffset != 1) {
                            ((Text) lastHoverView).setTextColor(textColor);
                        }
                        lastHoverView = itemView;
                    }
                    if (lastHoverView != null && (lastHoverView.getTop() > touchY || lastHoverView.getBottom() < touchY)) {
                        ((Text) lastHoverView).setTextColor(textColor);
                    }
                }

                private void animateOut(Component view) {
                    if (view == null) {
                        return;
                    }
                    text = (Text) view;
                    text.setTextColor(textColor);
                    AnimatorProperty translationX = new AnimatorProperty(view);
                    translationX.moveFromX(-spacing);
                    translationX.moveToX(150);
                    translationX.setDuration(200);
                    translationX.start();
                }

                private void animateIn(Component view) {
                    AnimatorProperty translationX = new AnimatorProperty(view);
                    translationX.moveFromX(0);
                    translationX.moveToX(-spacing);
                    translationX.setDuration(200);
                    translationX.start();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NotExistException e) {
            e.printStackTrace();
        } catch (WrongTypeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onClick(Component component) {
        switch (component.getId()) {
            case ResourceTable.Id_btn_switch:
                if (fantasyDrawerLayout.getOpenLeftMenu() == false) {
                    AnimatorProperty animatorProperty = new AnimatorProperty();
                    animatorProperty.setTarget(mainImage)
                            .setDuration(100)
                            .moveToX(leftMenu.getWidth())
                            .start();
                    leftMenu.setVisibility(Component.VISIBLE);
                    fantasyDrawerLayout.setOpenLeftMenu(true);
                    btnSwitch.setBackground(new PixelMapElement(
                            getPixelMapFromResource(this, ResourceTable.Media_return)));
                } else {
                    AnimatorProperty animatorProperty = new AnimatorProperty();
                    animatorProperty.setTarget(mainImage)
                            .setDuration(100)
                            .moveFromX(leftMenu.getWidth())
                            .moveToX(0)
                            .start();
                    if (!animatorProperty.isRunning()) {
                        leftMenu.setVisibility(Component.INVISIBLE);
                    }
                    fantasyDrawerLayout.setOpenLeftMenu(false);
                    btnSwitch.setBackground(new PixelMapElement(
                            getPixelMapFromResource(this, ResourceTable.Media_menu)));
                }
                break;
            default:
                if (component instanceof Text) {
                    String title = ((Text) component).getText().toString();
                    if (title.startsWith("星期")) {
                        new ToastDialog(this)
                                .setText(title)
                                .setDuration(0)
                                .show();
                    } else {
//                startActivity(UniversalActivity.newIntent(this, title));
                        new ToastDialog(this)
                                .setText(title)
                                .setDuration(0)
                                .show();
                    }
                } else if (component.getId() == ResourceTable.Id_userInfo) {
//            startActivity(UniversalActivity.newIntent(this, "个人中心"));
                    new ToastDialog(this)
                            .setText("个人中心")
                            .setDuration(0)
                            .show();
                }
                break;
        }

    }

    /**
     * 通过资源ID获取位图对象
     *
     * @param context
     * @param resourceId
     * @return PixelMap
     */
    private PixelMap getPixelMapFromResource(Context context, int resourceId) {
        InputStream inputStream = null;
        try {
            // 创建图像数据源ImageSource对象
            inputStream = context.getResourceManager().getResource(resourceId);
            ImageSource.SourceOptions srcOpts = new ImageSource.SourceOptions();
            srcOpts.formatHint = "image/jpg";
            ImageSource imageSource = ImageSource.create(inputStream, srcOpts);
            // 设置图片参数
            ImageSource.DecodingOptions decodingOptions = new ImageSource.DecodingOptions();
            return imageSource.createPixelmap(decodingOptions);
        } catch (IOException | NotExistException e) {
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                }
            }
        }
        return null;
    }
}
