package com.github.mzule.fantasyslide;

import com.github.mzule.fantasyslide.slice.MainAbilitySlice;
import com.github.mzule.fantasyslide.slice.MainActivity;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class MainAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(MainActivity.class.getName());
    }
}
