# FantasySlide
本项目是基于开源项目FantasySlide进行harmonyos化的移植和开发的，可以通过项目标签以及
[github地址](https://github.com/mzule/FantasySlide).


移植版本：源master版本

## 项目介绍
### 项目名称：FantasySlide
### 所属系列：harmonyos的第三方组件适配移植
### 功能：
    一次手势完成滑出侧边栏与选择菜单功能实现。


### 项目移植状态：完全移植
### 调用差异：基本没有使用差异，请参照demo使用
### 原项目Doc地址：https://github.com/mzule/FantasySlide
### 编程语言：java

### 项目截图（涉及文件仅供demo测试使用）

![鸿蒙运行效果](art/shotScreen.png)  
![demo运行效果](art/demo.gif)


## 安装教程
    
#### 方案一
建议下载开源代码并参照demo引入相关库：
```groovy
 dependencies {
    implementation project(':library')
 }  
```

#### 方案二
* 项目根目录的build.gradle中的repositories添加：
```groovy
  buildscript {
       repositories {
           ...
           mavenCentral()
       }
       ...
   }
   
   allprojects {
       repositories {
           ...
           mavenCentral()
       }
   }
```
* module目录的build.gralde中dependencies添加：
```groovy
dependencies {
     implementation 'com.gitee.ts_ohos:fantasySlideLibrary:1.0.1' 
 }
```

## 使用说明
创建布局
```xml
<com.github.mzule.fantasyslide.FantasyDrawerLayout
        ohos:id="$+id:drawerLayout"
        ohos:below="$id:layout_1"
        ohos:height="match_parent"
        ohos:width="match_parent">
    
        <!-- 这里是 左侧边栏-->
        <com.github.mzule.fantasyslide.SideBar
            xmlns:hap="http://schemas.huawei.com/res/ohos-auto"
            ohos:id="$+id:leftSideBar"
            ohos:height="match_parent"
            ohos:width="180vp"
            ohos:alignment="vertical_center"
            ohos:background_element="$color:colorPrimary"
            ohos:visibility="invisible"
            hap:maxTranslationX="44vp">

            <!-- 这里是 SideBar 的子视图-->
            
        </com.github.mzule.fantasyslide.SideBar>

        <!-- 这里是 右侧边栏-->
        <com.github.mzule.fantasyslide.SideBar
            xmlns:hap="http://schemas.huawei.com/res/ohos-auto"
            ohos:id="$+id:rightSideBar"
            ohos:height="match_parent"
            ohos:width="133vp"
            ohos:alignment="vertical_center|right"
            ohos:visibility="invisible"
            ohos:background_element="$color:LightSkyBlue"
            hap:maxTranslationX="-33vp">

            <!-- 这里是 SideBar 的子视图-->

        </com.github.mzule.fantasyslide.SideBar>

        <!-- 这里是 主界面-->
        <Image
            ohos:id="$+id:mainImage"
            ohos:height="match_parent"
            ohos:width="match_parent"
            ohos:scale_mode="clip_center"
            ohos:image_src="$media:fake" />

    </com.github.mzule.fantasyslide.FantasyDrawerLayout>
```

1. 最外层的 FantasyDrawerLayout 是自定义布局，按照左、右侧边栏和主界面排列。
2. SideBar 用来包装每一个菜单项，SideBar 本质上可以当做一个 vertical 的 LinearLayout 来使用。
3. 通过设置 maxTranslationX 可以设置菜单项动画的最大位移。左边的侧边栏 maxTranslationX 为正数，右边的侧边栏 maxTranslationX 为负数。


License
--------
Apache License  2.0

