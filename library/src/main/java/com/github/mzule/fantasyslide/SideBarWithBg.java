package com.github.mzule.fantasyslide;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.AttrSet;
import ohos.agp.components.DependentLayout;
import ohos.agp.components.SlideDrawer;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

/**
 * 封装 SideBar 以及 SideBarBgView,保证 SideBarBgView 可以展示在 SideBar 背后
 * Created by CaoDongping on 9/6/16.
 */
class SideBarWithBg extends DependentLayout {

    private SideBarBgView bgView;
    private SideBar sideBar;

    public SideBarWithBg(Context context) {
        super(context);
    }

    public SideBarWithBg(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public static SideBarWithBg wrapper(SideBar sideBar) {
        SideBarWithBg layout = new SideBarWithBg(sideBar.getContext());
        layout.init(sideBar);
        return layout;
    }

    private void init(SideBar sideBar) {
        setLayoutConfig(sideBar.getLayoutConfig());

        LayoutDirection layoutDirection = getLayoutConfig().getLayoutDirection();
//        int direction = sideBar.getDirection();
        this.sideBar = sideBar;
        bgView = new SideBarBgView(getContext());
//        bgView.setParentLayoutGravity(layoutDirection);
        addComponent(bgView, 0,
                new LayoutConfig(LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT));
        addComponent(bgView);

        // 将背景色转移给 bgView, 并清除 sideBar 自身的背景色
        bgView.setDrawable(sideBar.getBackgroundElement());
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(Color.TRANSPARENT.getValue()));
        sideBar.setBackground(shapeElement);
//        sideBar.setParentLayoutGravity(layoutDirection);

//        addComponent(sideBar, LayoutConfig.MATCH_CONTENT, LayoutConfig.MATCH_CONTENT);
        addComponent(sideBar);
    }

    void setTouchY(float y, float percent, boolean isLeft) {
        bgView.setTouchY(y, percent, isLeft);
        sideBar.setTouchY(y, percent, isLeft);
    }

    public void onMotionEventUp() {
        sideBar.onMotionEventUp();
    }
}
