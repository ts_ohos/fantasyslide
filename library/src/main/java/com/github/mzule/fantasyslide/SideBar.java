package com.github.mzule.fantasyslide;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import java.io.IOException;

/**
 * Created by CaoDongping on 9/6/16.
 */
public class SideBar extends DirectionalLayout {
    private LayoutDirection parentLayoutGravity;
    private boolean opened;
    private Transformer transformer;
    private FantasyListener fantasyListener;
    private int maxTranslationX;
    private int DIRECTION = SideGravity.LEFT;

    public SideBar(Context context) {
        this(context, null);
    }

    public SideBar(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public SideBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        init(attrSet);
    }

    private void init(AttrSet attrs) {
        setOrientation(VERTICAL);
        if (attrs != null) {
            if (attrs.getAttr("maxTranslationX").isPresent()) {
                maxTranslationX = attrs.getAttr("maxTranslationX").get().getDimensionValue();
            } else {
                try {
                    maxTranslationX = getResourceManager()
                            .getElement(ResourceTable.Integer_sideBar_TranslationX).getInteger();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (NotExistException e) {
                    e.printStackTrace();
                } catch (WrongTypeException e) {
                    e.printStackTrace();
                }
            }
            transformer = new DefaultTransformer(maxTranslationX);
        }
    }

    void setTouchY(float y, float percent, boolean isLeft) {
        opened = percent == 1;
        int childCount = getChildCount();
        boolean found = false;
        for (int i = 0; i < childCount; i++) {
            Component child = getComponentAt(i);
            child.setPressState(false);
            boolean inHover = opened && child.getTop() < y && child.getBottom() > y;
            if (inHover) {
                found = true;
                if (fantasyListener == null || !fantasyListener.onHover(child, i)) {
                    child.setPressState(true);
                }
            }
            if (percent != 1) {
                Text text;
                if (child instanceof DirectionalLayout) {
                    DirectionalLayout layout = (DirectionalLayout) child;
                    text = (Text) layout.getComponentAt(1);
                } else {
                    text = (Text) child;
                }
                if (isLeft) {
                    text.setTextColor(Color.WHITE);
                } else {
                    text.setTextColor(Color.BLACK);
                }
            }
            transformer.apply((ComponentContainer) getComponentParent(), child, y, percent, isLeft);
        }
        if (opened && !found && fantasyListener != null) {
            fantasyListener.onHover(null, -1);
        }
    }

    void onMotionEventUp() {
        for (int i = 0; opened && i < getChildCount(); i++) {
            Component child = getComponentAt(i);
            if (child.isPressed()) {
                if (fantasyListener == null || !fantasyListener.onSelect(child, i)) {
                    child.simulateClick();
                    if (getDirection() == SideGravity.RIGHT) {
                        Text childText = (Text) child;
                        childText.setTextColor(Color.BLACK);
                    } else {
                        Text text;
                        if (child instanceof DirectionalLayout) {
                            DirectionalLayout layout = (DirectionalLayout) child;
                            text = (Text) layout.getComponentAt(1);
                        } else {
                            text = (Text) child;
                        }
                        text.setTextColor(Color.WHITE);
                    }
                }
                return;
            }
        }
        if (fantasyListener != null) {
            fantasyListener.onCancel();
        }
    }

    void setParentLayoutGravity(LayoutDirection parentLayoutGravity) {
        this.parentLayoutGravity = parentLayoutGravity;
    }

    public void setTransformer(Transformer transformer) {
        this.transformer = transformer;
    }

    public void setFantasyListener(FantasyListener fantasyListener) {
        this.fantasyListener = fantasyListener;
    }

    public int getDirection() {
        return DIRECTION;
    }

    public void setDirection(int direction) {
        DIRECTION = direction;
    }
}
