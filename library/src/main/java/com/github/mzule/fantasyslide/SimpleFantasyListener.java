package com.github.mzule.fantasyslide;


import ohos.agp.components.Component;

/**
 * Created by CaoDongping on 9/7/16.
 */

public class SimpleFantasyListener implements FantasyListener {
    @Override
    public boolean onHover(Component view, int index) {
        return false;
    }

    @Override
    public boolean onSelect(Component view, int index) {
        return false;
    }

    @Override
    public void onCancel() {

    }
}
