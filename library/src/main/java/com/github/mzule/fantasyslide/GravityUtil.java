package com.github.mzule.fantasyslide;

import ohos.agp.components.Component;
import ohos.agp.components.SlideDrawer;

/**
 * Created by CaoDongping on 9/7/16.
 */
class GravityUtil {

    static boolean isLeft(Component.LayoutDirection gravity) {
        return gravity == Component.LayoutDirection.LTR ||
                gravity == Component.LayoutDirection.INHERIT;
    }

    static boolean isLeft(Component view) {
        return isLeft(getGravity(view));
    }

    static boolean isRight(Component.LayoutDirection gravity) {
        return gravity == Component.LayoutDirection.RTL ||
                gravity == Component.LayoutDirection.LOCALE;
    }

    static boolean isRight(Component view) {
        return isRight(getGravity(view));
    }

    //TODO 2021/8/31
    static Component.LayoutDirection getGravity(Component view) {
        if (view.getLayoutConfig() instanceof SlideDrawer.LayoutConfig) {
            return view.getLayoutConfig().getLayoutDirection();
        }
        throw new IllegalArgumentException("Not child of DrawerLayout");
    }
}
